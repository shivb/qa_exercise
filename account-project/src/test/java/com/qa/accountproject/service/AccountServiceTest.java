package com.qa.accountproject.service;

import com.qa.accountproject.BaseTest;
import com.qa.accountproject.model.Account;
import com.qa.accountproject.stub.AccountStub;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;

import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.BDDMockito.given;

@SpringBootTest
public class AccountServiceTest extends BaseTest {

    @Autowired
    private IAccountService accountService;

    @MockBean
    private AccountStub accountStub;

    private List<Account> accountList;

    @BeforeEach
    public void setup(){
        accountList = IntStream.range(0, 3)
                .mapToObj(i -> new Account(i, "acc_firstname_" + i, "acc_secondname_" + i, "123" + i))
                .collect(Collectors.toList());
    }

    @Test
    public void test_getAllAccounts(){
        given(accountStub.findAll()).willReturn(accountList);
        assertThat(accountService.findAllAccounts()).hasSize(3);
    }


    @Test
    public void test_CreateNewAccount(){
        Account newAcc = new Account(1, "acc_fName" , "acc_sName", "1234");
        given(accountStub.addAccount(newAcc)).willReturn(newAcc);
        assertThat(accountService.createAccount(newAcc)).isEqualTo(newAcc);
    }


    @Test
    public void test_deleteAccount(){
        Account account = new Account(1, "acc_fName" , "acc_sName", "1234");
        given(accountStub.deleteAccount(1)).willReturn(account);
        assertThat(accountService.deleteAccount(account.getId())).isEqualTo(account);
    }


}
