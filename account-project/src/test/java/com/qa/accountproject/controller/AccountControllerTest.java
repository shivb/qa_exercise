package com.qa.accountproject.controller;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.qa.accountproject.BaseTest;
import com.qa.accountproject.model.Account;
import com.qa.accountproject.service.IAccountService;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;

import java.util.Arrays;
import java.util.List;

import static org.mockito.ArgumentMatchers.anyInt;
import static org.mockito.BDDMockito.given;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;


@WebMvcTest(value = AccountController.class)
public class AccountControllerTest extends BaseTest {

    public static final String $_MESSAGE = "$.message";
    @Autowired
    private MockMvc mockMvc;

    @Value("${url.account.json}")
    private String url;


    @Value("${message.acc.create.success}")
    private String createSuccessMsg;


    @Value("${message.acc.delete.success}")
    private String delSuccessMsg;


    private ObjectMapper mapper = new ObjectMapper();

    private Account account;

    @MockBean
    private IAccountService service;


    @BeforeEach
    public void setup(){
        account = new Account(1, "acc_fName" , "acc_sName", "1234");

    }

    @Test
    public void test_getAccounts() throws Exception {

        List<Account> accounts = Arrays.asList(account);
        given(service.findAllAccounts()).willReturn(accounts);

        mockMvc.perform(get(url).accept(MediaType.APPLICATION_JSON_UTF8))
                .andExpect(status().isOk()).andExpect(content().json(mapper.writeValueAsString(accounts)));

    }

    @Test
    public void test_createAccount() throws Exception {

        given(service.createAccount(account)).willReturn(account);

        String json = mapper.writeValueAsString(account);
        mockMvc.perform(post(url)
                .contentType(MediaType.APPLICATION_JSON)
                .content(json)
                .accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk()).andExpect(MockMvcResultMatchers.jsonPath($_MESSAGE).value(createSuccessMsg));
    }


    @Test
    public void test_DeleteAccount() throws Exception {

        given(service.deleteAccount(account.getId())).willReturn(account);

        String json = mapper.writeValueAsString(account);
        mockMvc.perform(delete(url+"/"+account.getId())
                .contentType(MediaType.APPLICATION_JSON)
                .content(json)
                .accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk()).andExpect(MockMvcResultMatchers.jsonPath($_MESSAGE).value(delSuccessMsg));
    }



    @Test
    public void test_getAccountById() throws Exception {

        given(service.findAccountByID(anyInt())).willReturn(account);

        mockMvc.perform(get(url+"/1").accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk()).andExpect(content().json(mapper.writeValueAsString(account)));

    }

}
