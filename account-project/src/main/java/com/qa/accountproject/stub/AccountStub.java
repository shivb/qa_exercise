package com.qa.accountproject.stub;

import com.qa.accountproject.model.Account;
import lombok.extern.log4j.Log4j2;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Profile;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.stream.Collectors;

/**
 * This stub returns static data of the Account Info
 */
@Profile("run-with-stub")
@Component
@Log4j2
public class AccountStub {
    private Map<Integer, Account> accounts = new HashMap<Integer, Account>();
    private AtomicInteger idGenerator = new AtomicInteger(0);

    /**
     * Initializes accounts map
     */

    @PostConstruct
    public void init() {

        List<Account> accounts = Arrays.asList(
                new Account(idGenerator.incrementAndGet(), "John", "Doe", "1234"),
                new Account(idGenerator.incrementAndGet(), "Jane", "Doe", "1235"),
                new Account(idGenerator.incrementAndGet(), "Jim", "Taylor", "1236")
        );
        log.info("Init AccountStub");
        this.accounts = accounts.stream().collect(
                Collectors.toMap(acc -> acc.getId(), acc -> acc));
    }


    /**
     * Return all Accounts
     * @return List
     */
    public List<Account> findAll() {
        return accounts.values().stream().collect(Collectors.toList());
    }


    /**
     * Adds new account
     * @param newAcc
     * @return
     */
    public Account addAccount(Account newAcc) {
        return accounts.putIfAbsent(newAcc.getId(),newAcc);
    }


    public Account findAccount(int id) {
        return accounts.get(id);
    }

    public Account deleteAccount(int id) {
        return accounts.remove(id);
    }
}