package com.qa.accountproject.model;

import lombok.Data;

/**
 * This is the model class for Account
 */
@Data
public class Account {

    private Integer id;

    private String firstName;

    private String secondName;

    private String accountNumber;

    public Account(Integer id, String firstName, String secondName, String accountNumber) {
        this.id = id;
        this.firstName = firstName;
        this.secondName = secondName;
        this.accountNumber = accountNumber;
    }

    //Note getter,setters, equals etc method will be implemented by Lombok
}
