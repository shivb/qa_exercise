package com.qa.accountproject.controller;

import com.qa.accountproject.model.Account;
import com.qa.accountproject.service.IAccountService;
import lombok.extern.log4j.Log4j2;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

import static com.fasterxml.jackson.databind.node.JsonNodeFactory.instance;
import static org.springframework.http.MediaType.APPLICATION_JSON_VALUE;

@RestController
@RequestMapping("${url.account.json}")
@Log4j2
public class AccountController {



    @Autowired
    IAccountService service;

    @Value("${message.acc.create.success}")
    private String createSuccessMsg;

    @Value("${message.acc.delete.success}")
    private String deleteSuccessMsg;

    /**
     * Returns all Accounts
     * @return List
     */
    @GetMapping
    public List<Account> getAccounts(){
        log.info("Retrieving all accounts");
        return service.findAllAccounts();
    }


    /**
     * Creates a new Account
     * @param account
     * @return Account
     */
    @PostMapping(produces = APPLICATION_JSON_VALUE)
    public ResponseEntity<String> createAccount(@RequestBody Account account){
        log.info("Creating a new Account:"+account);
        service.createAccount(account);
        return ResponseEntity.ok(instance.objectNode().put("message", createSuccessMsg).toString());
    }


    /**
     * Delete a Account
     * @param id
     * @return deleted Account
     */
    @DeleteMapping("/{id}")
    public ResponseEntity<String> deleteAccount(@PathVariable("id") Integer id){
        log.info("Deleting account by id: "+id);
        service.deleteAccount(id);
        return ResponseEntity.ok(instance.objectNode().put("message", deleteSuccessMsg).toString());
    }


    /**
     * Return a Account with the requested account number
     * @param id
     * @return Account
     */
    @GetMapping("/{id}")
    public Account getAccountByAccID(@PathVariable Integer id){
        return service.findAccountByID(id);
    }
    
}
