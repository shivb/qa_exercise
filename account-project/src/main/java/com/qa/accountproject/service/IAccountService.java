package com.qa.accountproject.service;

import com.qa.accountproject.model.Account;

import java.util.List;

public interface IAccountService {
    public List<Account> findAllAccounts() ;

    public Account createAccount(Account newAcc);

    public Account deleteAccount(Integer id);

    public Account findAccountByID(Integer id);
}
