package com.qa.accountproject.service;

import com.qa.accountproject.model.Account;
import com.qa.accountproject.stub.AccountStub;
import lombok.extern.log4j.Log4j2;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
@Log4j2
public class AccountService implements IAccountService{

    @Autowired
    AccountStub stub;

    @Override
    public List<Account> findAllAccounts() {
        log.info("Retrieving all accounts");
        return stub.findAll();
    }

    @Override
    public Account createAccount(Account newAcc) {
        log.info("Creating new Account: "+newAcc);
        return stub.addAccount(newAcc);

    }

    @Override
    public Account deleteAccount(Integer id) {
        log.info("Deleting account by id: "+id);
        return stub.deleteAccount(id);
    }

    @Override
    public Account findAccountByID(Integer id) {
        log.info("Find account by id: "+id);
        return stub.findAccount(id);
    }
}
